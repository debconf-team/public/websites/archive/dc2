
policy-rc.d  (init script policy control)



Used by invoke-rc.d to define policy
No sample implementations thus far
Avoid start/stop of daemons in chroot jails
Enforces any weird policy the local system administrator might want
Allows fallback actions

