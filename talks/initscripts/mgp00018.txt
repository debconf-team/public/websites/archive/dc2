
LSB init scripts


The LSB:
Defines actions (start, stop, restart, force-reload, reload, status)
Defines exit codes (Debian's are mostly compatible)
Mandates that all error messages go to stderr
Mandates that all status/informational messages go to stdout

Only exit status 5 in LSB (service is not available) is incompatible with Debian policy (we return status zero and do nothing)

