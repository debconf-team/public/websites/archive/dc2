
Simpleinit (1/2)


Written by Richard Gooch, which wrote much about dependency-based systems

The need(8) concept:
Dynamic dependency system
Call "need <facility>" before you do any system state changes
"need" will ensure that <facility> is available
Supports "provide <facility>" as well, on wiggy's request

Shutdown mechanics:
Store dependency information as we start services
Reverse list on shutdown (rollback)

