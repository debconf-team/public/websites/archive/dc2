
Alternative init script systems (1/2)


runit
Works using DJB's daemontools
Debian contrib material: why bother?


twsinit
Very tiny init, 4kb memory footprint


Busybox
Simple, sysvinit compatible
Very useful for boot floppies, Debian's use it
Does not implement all of sysvinit features

