
How You Can Participate!

Acquire an S-band downconverter and antenna, IF receiver, 
and a PC with sound card and help acquire telemetry from 
the IHU!

Get a ham radio license, and you can uplink... :-)

Join AMSAT and help sponsor the ongoing operational 
costs for AO-40, and help fund our next project!

