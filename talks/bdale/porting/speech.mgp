%% Avoiding i386 Bigotry - Debconf 2 Update, 6 July 2002
%% Copyright 2002 Bdale Garbee <bdale@gag.com>
%%
%% Define the fonts we will use
%%
%deffont "standard" tfont "Arial.ttf"
%deffont "typewriter" tfont "Times_New_Roman.ttf"
%deffont "fixed" xfont "terminal"
%%
%% Default settings for special lines
%%
%default 1 leftfill, fore "black", back "white", bimage "background.bmp"
%default 2 size 7, vgap 10, prefix " ", font "standard"
%default 3 size 2, bar "gray70", vgap 10
%default 4 size 5, vgap 30, font "standard"
%%
%% Default settings for indented lines
%%
%tab 1 size 5, vgap 40, prefix "  ", icon box "green" 50
%tab 2 size 4, vgap 40, prefix "      ", icon arc "red" 50
%tab 3 size 3, vgap 40, prefix "            ", icon delta3 "blue" 40
%%
%%%%%%%%%%%%%%%%%%
%page
%nodefault, font "standard", fore "black", back "white", bimage "background.bmp"

%center
Coping With i386 Bigotry 

Lessons Learned Porting 
Debian GNU/Linux to 
Five Other Architectures

%size 6
Bdale Garbee
%size 5
<bdale@gag.com>


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page

Objectives for Today

One of Debian's Core Values is Portability

	Debian: The Universal Operating System

	More Ports than Any Other Distro

Working on Portability Issues Improves our Code Quality


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page

Agenda


Personal Observations on the Evolution of Portability

Differences in PARISC and IA-64 Ports

How I ended up working for HP again...

Problems Facing Debian Porters 

How Developers and Package Maintainers Can Help

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page

A Personal History Porting...

	RCA 1802 and the ACE
		commonality of systems, EFI maps differed

	Z80's and CP/M
		BIOS and BDOS standardized
		custom I/O addresses for each system

	Early'ish Days of UNIX
		is it "BSD" or "SYSV"
		serial interface issues
		HP-UX with BSD guts and SVID Compliance

	Emergence of Free OS Community
		Almost Entirely on IA-32 Systems
		Minix -> 386BSD -> BSDI -> Linux

	Debian
		ia32 was "home", m68k was the first "port"
		but... Debian now runs on something like 11 architectures!
		Debian even runs on more than one kernel...

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page

PARISC Port History


Only Major 32-bit CPU Family NOT Running Linux
Various Internal Discussions at HP
The Puffin Group, Oct 1998
HP Officially Agrees to Participate, March 1999
Linuxcare Acquires the Puffins, Dec 1999
	No Longer Just a Fun Project...
	HP+Linuxcare Contract to Continue Porting
HP Wants a Complete Distribution

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page

HP picks Debian

Why?

	PARISC Port Irrelevant Without a Distribution
	Public message is that Itanium replaces PARISC
	HP Thinks of PARISC Port as a Community Activity
		Learning How to Work With/In Community
		Hacker, not Business Driven
	Commercial distributors want "real money"
	Debian Perceived as a "Commons"
		Community Development Model Instantiated
		No One Business In Control
		Companies can Invest w/o Contracts
		Ability to Influence Future Direction
	Key Engineers at HP and Linuxcare Liked Debian

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page

HP picks Debian

How?

	No Forks!
		"Real" Debian, NOT a Derivative
		Merge All Code Upstream as Quickly as Possible
	Continue Linuxcare Contract
	Hire Existing Debian Developers
	Encourage HP Engineers to Join Debian
	Selected Investments in Features
	Improve Debian Initial Customer Experience

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page

IA-64 Port History


Large Industry Focus on IA-64

HP Already a Significant Contributor 
	HP Labs Hosting Key Contributors
		David Mosberger - principle kernel maintainer
		Stephane Eranian - gnu-efi, elilo, pfmon, etc
	Co-Developer with Intel of the Architecture

Commercial Distributions had a Head-Start
	TL Worked, but was Rough
	RH, SuSE, Caldera Early Betas

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page

More IA-64 Port History


Before Debian had Hardware
	Playing with "Public" Systems Hosted by SourceForge
	Initial Debian Packages Built
	Needed Hardware to Progress Further

HP Provides a BigSur
	HP Loans Agilent 3 Systems, 2 HP-UX + 1 Linux
	One Ends up in Bdale's Cubicle...
	Bdale Takes One "so Tausq can Play with a Machine"
	Compiled in a chroot Until System Self-Hosting
	Bdale Starts an Autobuilder...
	HP Hires Bdale, Sends out Lots of Hardware...


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page

Current Status


PARISC 
	Over 90% of Packages Built and Current in Debian
	0.9.3 Install Media Released 11 Dec 2001
	Will Release with Woody (Debian 3.0)

IA-64 
	Over 95% of Packages Built and Current in Debian
	Debian Installer-Only CD
		Released Several Months Ago
		Updated several times (those pesky Itanium 2 guys...)
	Will Release with Woody (Debian 3.0)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page

So, What are the Problems?

Not All Modern CPUs are 32 bits!
	ia64 is LP64 -> ints 32 bits, longs + ptrs 64 bits

New Archs MUST Track New Kernels, Toolchain Revs
	ia64 using gcc-2.96, hppa gcc-3.X
	2.2 vs 2.4 kernels, installer issues

Mixing PIC and non-PIC Code at Runtime
	a linker issue?  sloppiness is not a virtue!

Not All Platforms Have All Features
	serial consoles
	no sound hardware, joysticks, etc


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page

LaMont's Favorites...

	"BUT IT WORKS ON MY PENTIUM..."
	config.{guess,sub} out of date
	sizeof(int) != sizeof(pointer)
	hppa == hp-ux (or mips == irix), etc.
	gcc (g++) 3.0 errors, particularly on i386
	non-PIC in shared objects, similar policy violations.
	assuming that char == [un]signed char. (s390)
	MU's that exclude the arch, rather than fixing it...
	Zero Tolerance for Porting Problems Policy

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page

How to Help

Implicit Declarations are BAD!

Own a Second Architecture, or use Debian's Machine Farm
	experience life on a non-default platform
	learn first-hand why programs don't compile or run
	help keep Debian vital on all these architectures!

Test Builds in a Minimal CHROOT Environment
	only way to really verify build-depends info correct
	autobuilders start with essential+build-essential
	pbuilder by Junichi Uekawa makes this pretty easy

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page

How NOT to Help...

%center
%size 10

Architecture: i386

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%page

To Learn More

%center
%size 8

http://www.debian.org/ports/hppa
http://www.debian.org/ports/ia64

http://buildd.debian.org/

