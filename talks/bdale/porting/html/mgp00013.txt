
How to Help

Implicit Declarations are BAD!

Own a Second Architecture, or use Debian's Machine Farm
experience life on a non-default platform
learn first-hand why programs don't compile or run
help keep Debian vital on all these architectures!

Test Builds in a Minimal CHROOT Environment
only way to really verify build-depends info correct
autobuilders start with essential+build-essential
pbuilder by Junichi Uekawa makes this pretty easy

