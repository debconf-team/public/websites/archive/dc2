<title>debconf overview</title>
<p align=center>
<a href="overview.html">&lt;&lt;&lt;</a>&nbsp;
<a href="design.html">&gt;&gt;&gt;</a>
<p>
<h1>Motivations for debconf</h1>
<ul>
<li>historically, packages prompted the user in an ad-hoc way
<small>
<ul>
<li>inconsistent ui (echo/read; dialog, etc)
<li>default values presented differently, lists of choices
presented differently, pause after messages done
differently (when it was done at all)
<li>no way to go back to previous questions
<li>prompting done in postinst so install had to be babysat
<li>frequent repetition of questions on upgrades
</ul>
</small>
<p>
<li>no way to do a noninteractive install
<small>
<ul>
<li>apt-get upgrade cron job thus impossible
<li>made cluster installs and automated installs a bear
<li>debian derivatives that don't want all those questions
had to do a lot of hacking
</ul>
</small>
<p>
<li>something better was needed, that could:
<small>
<ul>
<li>use a consistent UI
<li>prioritize questions, and only show important ones
sometimes
<li>easily ask questions only once
<li>store answers in a central database for large installations
</ul>
</small>
</ul>
