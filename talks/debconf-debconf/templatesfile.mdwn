<title>Debconf templates file example</title>
<p align=center>
<a href="howitworks.html">&lt;&lt;&lt;</a>
<h1>Debconf templates file example</h1>
<small>
<pre>
Template: demodb/adminuser
Type: string
Default: dbadmin
Description: What user will administer the database?
You can choose what user account is used to administer 
the database; this user will be created if it does not
already exist.
Template: demodb/dbtype
Type: select
Choices: red-green, black-red, btree
Description: Select database type:
Um, is this supposed to mean something?
Template: demodb/removedb
Type: boolean
Default: true
Description: Do you want to remove the database?
You are purging the demodb package from your system. Should the database
be removed as well?
</pre>
</small>
