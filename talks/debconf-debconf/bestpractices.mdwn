<title>best practices</title>
<p align=center>
<a href="overview.html">&lt;&lt;&lt;</a>&nbsp;
<a href="spot-the-bug.html">&gt;&gt;&gt;</a>
<h1>best practices</h1>
Using debconf is fairly simple, but there are some non-obvious
catches and frequent mistakes, and some useful techniques.
<h1>common mistakes</h1>
<ul>
<li>accidentally putting config script stuff in postinst
<li>the inverse
<li>forgetting to source confmodule in postinst
<li>not sourcing confmodule early enough
<li>abuse of seen flag
<li>using a select data type when a boolean would do
<li>accidental "unknown" owners for questions that don't go away
right
</ul>
<p>
